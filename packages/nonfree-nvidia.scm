(define-module (elais packages nvidia)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system linux-module)
  #:use-module (guix build-system trivial)
  #:use-module (nongnu linux-firmware))

(define-public nvidia-firmware
  (package
   (inherit linux-firmware)
   (name "nvidia-firmware")
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((source (assoc-ref %build-inputs "source"))
               (fw-dir (string-append %output "/lib/firmware"))
               (bin-dir (string-append fw-dir "/nvidia")))
          (mkdir-p bin-dir)
          (copy-recursively (string-append source "/nvidia") bin-dir)
          (install-file (string-append source "/LICENSE.nvidia") fw-dir)
          #t))))
   (home-page "https://www.nvidia.com/object/unix.html")
   (synopsis "Nonfree firmware for Nvidia graphics chips")
   (description "Nonfree firmware for Nvidia graphics chips. Many modern
cards require a nonfree kernel module to run properly.")
   (license
    (nonfree
     (string-append
      "https://git.kernel.org/pub/scm/linux/kernel/git/firmware"
      "/linux-firmware.git/plain/LICENSE.nvidia")))))
